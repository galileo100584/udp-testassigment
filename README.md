# UDP Værmelding Applikasjon

Lag en Spring-basert java applikasjon som skal kunne kjøres fra en jar-fil. Denne applikasjonen skal inneholde en scheduled job som sender ut en 
mail 1 gang i timen med informasjon om hva slags vær det er på Svalbard (hentet fra yr.no).

### Systemkrav
Du trenger java 8 (JDK8) og maven 4 for å bygge og kjøre prosjektet. Dette kan du sjekke med følgende kommando i en terminal:

```
java -version
```

```
mvn -version
```


### Instalering
For å installer applikasjonen, bruk følgende kommando i en terminal på root nivå av prosjektet.

```
mvn clean install
```

## Kjør applikasjonen
For å teste applikasjonen bruk følgende kommando:

```
java -jar target/gs-scheduling-tasks-0.1.0.jar 
```


## Bygget med

* Maven 4
* Spring/ spring-boot 1.4.1