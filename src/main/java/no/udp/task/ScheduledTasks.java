package no.udp.task;

//java pakker:
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

//Spring pakker:
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

    //10000*6*5 = 5 minutter, 10000*6*60 = 1 time, i denne sammenhengen
    @Scheduled(fixedRate = 10000*6*5)
    public void sendMailToCustomers() {

        //Henter ut sping konfigurasjonen:
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-mail.xml");

        XmlParser weather = new XmlParser();
        String msg = null;
        try {
            msg = weather.forecast();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Fetching data from yr.no");
		String email_adr = "post@larserikopdal.no"; 
		String subject= "Forecast from svalbard";

        //Lager epost-objekt med innhold; mottaker, melding og emne:
        MailContent mailContent = new MailContent(email_adr, msg, subject);
        System.out.println("Sending email to "+email_adr+ " " + dateFormat.format(new Date())); 
		System.out.println("Subject: " + subject);


        //Henter konfigurasjon for epost tjeneste
        MailService mailService = (MailService) context.getBean("mailService");

        //Sender epost, sendmail er driver for send metoden i Spring klassen MailSender.
        mailService.sendMail(mailContent);
    }
}
