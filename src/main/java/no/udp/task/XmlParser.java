package no.udp.task;
//Java pakker:
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

//Pakker for DOM parsing
import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

//Denne brukes for å fjerne html tagger i tekst:
import org.jsoup.Jsoup;

public class XmlParser {
     //Alle metoder med "protected" under er henten fra et eksempel på internet.
     protected Node getNode(String tagName, NodeList nodes) {
        for ( int x = 0; x < nodes.getLength(); x++ ) {
            Node node = nodes.item(x);
            if (node.getNodeName().equalsIgnoreCase(tagName)) {
                return node;
            }
        }
        return null;
    }

     protected String getNodeValue(String tagName, NodeList nodes ) {
        for ( int x = 0; x < nodes.getLength(); x++ ) {
            Node node = nodes.item(x);
            if (node.getNodeName().equalsIgnoreCase(tagName)) {
                NodeList childNodes = node.getChildNodes();
                for (int y = 0; y < childNodes.getLength(); y++ ) {
                    Node data = childNodes.item(y);
                    if ( data.getNodeType() == Node.TEXT_NODE )
                        return data.getNodeValue();
                }
            }
        }
        return "";
    }

    //Denne metoden har jeg skrevet selv:
    String forecast() throws IOException{
        String fileName = "varsel2.xml";
        URL link = new URL("http://www.yr.no/sted/Norge/Svalbard/Svalbard/varsel.xml");

        InputStream in = new BufferedInputStream(link.openStream());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        int n = 0;
        while (-1!=(n=in.read(buf)))
        {
            out.write(buf, 0, n);
        }
        out.close();
        in.close();
        byte[] response = out.toByteArray();

        FileOutputStream fos = new FileOutputStream(fileName);
        fos.write(response);
        fos.close();

        String textOnly=null;

        try {
            DOMParser parser = new DOMParser();
            parser.parse("varsel2.xml");
            Document doc = parser.getDocument();

            // Henter root-noden i xml filen
            NodeList root = doc.getChildNodes();

            // Navigerer dypere ned for å få tilgang til ønsket data
            Node comp = getNode("weatherdata", root);
            Node exec = getNode("forecast", comp.getChildNodes() );

            Node text = getNode("text",exec.getChildNodes());

            Node location = getNode("location",text.getChildNodes());

            Node time = getNode("time",location.getChildNodes());

            //Her er noden vi søker:
            NodeList nodes = time.getChildNodes();
            String body = getNodeValue("body",nodes);
            textOnly = Jsoup.parse(body.toString()).text();
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
        return textOnly;
    }
}
