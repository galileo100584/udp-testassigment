package no.udp.task;
//En helt standard klasse for epost innhold med get'ers og set'ers for de private variablene:
public class MailContent {

    private String receiver;
    private String body;
    private String subject;

    public MailContent(String receiver, String body, String subject) {
        this.receiver = receiver;
        this.body = body;
        this.subject = subject;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}

