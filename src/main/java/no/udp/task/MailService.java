package no.udp.task;

//Spring pakker for epost sending:
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

public class MailService {

    private MailSender mailSender;


    public void sendMail(MailContent mailContent){
        //Lager nytt epost-objekt
        SimpleMailMessage mailMessage=new SimpleMailMessage();

        //Bruker get'er pga. private variabler i MailContent klassen. Det er slik man alltid gjøre det i Java.
		String reciver = mailContent.getReceiver();
		String subject = mailContent.getSubject();
		String body = mailContent.getBody();

        //Bruker set'er pga. private variabler i MailContent klassen. Det er slik man alltid gjøre det i Java.
        mailMessage.setTo(reciver);
        mailMessage.setSubject(subject);
        mailMessage.setText(body);

		//Sender epost med send metode i MailSender klassen
        mailSender.send(mailMessage);
    }
    public void setMailSender(MailSender mailSender) {
        this.mailSender = mailSender;
    }
}
