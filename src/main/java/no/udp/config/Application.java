package no.udp.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ComponentScan({ "no.udp.task" })
public class Application {



    public static void main(String[] args) {

        SpringApplication.run(new Object[] { Application.class }, args);

    }

}